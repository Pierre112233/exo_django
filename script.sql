--
-- Name: map_layer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.map_layer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.map_layer_id_seq OWNER TO postgres;


--
-- Name: map_layer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.map_layer_id_seq OWNED BY public.map_layer.id;


--
-- Name: map_layer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.map_layer ALTER COLUMN id SET DEFAULT nextval('public.map_layer_id_seq'::regclass);


--
-- Name: map_layer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.map_layer_id_seq', 1, true);



--
-- Name: map_field_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.map_field_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.map_field_id_seq OWNER TO postgres;


--
-- Name: map_field_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.map_field_id_seq OWNED BY public.map_field.id;


--
-- Name: map_field id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.map_field ALTER COLUMN id SET DEFAULT nextval('public.map_field_id_seq'::regclass);


--
-- Name: map_layer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.map_field_id_seq', 2, true);